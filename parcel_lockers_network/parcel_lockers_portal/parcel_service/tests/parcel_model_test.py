from django.test import TestCase
from parcel_service.models import *

class ParcelModelTest(TestCase):

    @classmethod

    def setUpTestData(cls):
        Parcel.objects.create(parcel_number='123456789012', user_id='1',
        order_taken_by_employee_id ="1",
        package_status_code ="1", package_details= "paczka ma zostać przekazana osobie XY", internal_package_flag = "True",
        kod_nadania = "810421",
        kod_odbioru = "810420",
        payment_status = "True",
        final_address = "1",
        parcel_locket_number = "32")

#1 parcel_number BigIntegerField
#2 user_id ForeignKey integer
#3 order_taken_by_employee_id ForeignKey integer
#4 package_status_code ForeignKey integer

#5 package_details CharField
    def test_package_details_label(self):
        parcel = Parcel.objects.get(id=5)  # type: Parcel
        field_label = parcel._meta.get_field('package_details').verbose_name
        self.assertEquals(field_label, 'package_details')

    def test_package_details_max_length(self):
        parcel = Parcel.objects.get(id=5)  # type: Parcel
        max_length = parcel._meta.get_field('package_details').max_length
        self.assertEquals(max_length, 300)

    def test_str(self):
        parcel = Parcel.objects.get(id=5)
        expected_object_name = parcel.name
        self.assertEquals(expected_object_name, str(parcel))

#6 internal_package_flag BooleanField

#7 kod_nadania CharField
    def test_kod_nadania(self):
        parcel = Parcel.objects.get(id=7)  # type: Parcel
        field_label = parcel._meta.get_field('kod_nadania').verbose_name
        self.assertEquals(field_label, 'kod_nadania')

    def test_kod_nadania_length(self):
        parcel = Parcel.objects.get(id=7)  # type: Parcel
        max_length = parcel._meta.get_field('kod_nadania').max_length
        self.assertEquals(max_length, 10)

    def test_str(self):
        parcel = Parcel.objects.get(id=7)
        expected_object_name = parcel.name
        self.assertEquals(expected_object_name, str(parcel))

#8 kod_odbioru CharField
    def test_kod_nadania(self):
        parcel = Parcel.objects.get(id=8)  # type: Parcel
        field_label = parcel._meta.get_field('kod_nadania').verbose_name
        self.assertEquals(field_label, 'kod_nadania')

    def test_kod_nadania_length(self):
        parcel = Parcel.objects.get(id=8)  # type: Parcel
        max_length = parcel._meta.get_field('kod_nadania').max_length
        self.assertEquals(max_length, 10)

    def test_str(self):
        parcel = Parcel.objects.get(id=8)
        expected_object_name = parcel.name
        self.assertEquals(expected_object_name, str(parcel))
        #linia poniżej idzie do ostatniego elementu badającego klasę
        with self.assertRaises(ValidationError):
            parcel.full_clean()
#9 payment_status BooleanField
#10 final_address ForeignKey
#11 parcel_locket_number IntegerField
