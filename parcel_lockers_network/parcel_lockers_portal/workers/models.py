from django.db import models
from django.contrib.postgres.fields import ArrayField
from parcel_service.models import Parcel

#Zarys nowego modelu aplikacji pracownikow wersja de facto trzecia

class WorkerMatrix(models.Model):

    take_matrix = ArrayField(models.IntegerField(null=True, blank=True, default=0), size=64)
    put_matrix = ArrayField(models.IntegerField(null=True, blank=True, default=0), size=64)

    def take_parcel(self):
        paczki = Parcel.objects.filter(order_taken_by_employee_id=id)
        i = 0
        for line in paczki.parcel_locket_number:
            take_matrix[i] = line

    def put_parcels(self):
        paczki = Parcel.objects.filter(order_send_by_employee_id=id)
        i = 0
        for line in paczki.parcel_locket_number:
            put_matrix[i] = line
