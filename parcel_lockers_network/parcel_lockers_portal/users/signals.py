#signals.py automatycznie tworzy profile dla uzytkownika razem z domyslnym obrazkiem

from django.db.models.signals import post_save
#post_save - po utworzeniu uzytkownika
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Profile

"""
*args (lub *) kiedy nie wiem ile argumentów jest przekazywanych do funkcji,
*args is used to send a non-keyworded variable length argument list to the function
ZA: https://book.pythontips.com/en/latest/args_and_kwargs.html
**kwargs to konwencja mozna uzyc (**)
**kwargs allows you to pass keyworded variable length of arguments to a function.
You should use **kwargs if you want to handle named arguments in a function. Here is an example to get you going with it:

**kwargs akceptuje jakiekolwiek dodatkowe keyword arguments
"""
#signal post_save
#instance to instancja usera
@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    instance.profile.save()
