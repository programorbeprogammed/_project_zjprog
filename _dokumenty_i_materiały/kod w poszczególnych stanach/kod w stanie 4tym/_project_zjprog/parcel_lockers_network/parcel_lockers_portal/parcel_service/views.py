#from django.http import HttpResponse

from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Parcel

"""
----------------------> DOKOŃCZ TEN WIDOK DANYMI I PACZKACH
"""

def home(request):
#   return HttpResponse('<h1>Strona główna</h1>')
    context = {
        'parcels': Parcel.objects.all()
    }
    return render(request, 'parcel_service/home.html', context)

def about(request):
#   return HttpResponse('<h1>Strona o</h1>')
    return render(request, 'parcel_service/about.html', {'title': 'About'})

class ParcelListView(ListView):
    model = Parcel
    template_name = 'parcel_service/home.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'parcels'
    ordering = ['-date costam']
    paginate_by = 7

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('-date_posted')
#to powyżej trzeba zmodyfikowac żeby nie było 404
#kiedy nie ma żadnej paczki


class PoarcelDetailView(DetailView):
    model = Parcel


#dokończ ten plik
