from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'

    def ready(self):
        import users.signals


#https://docs.djangoproject.com/en/2.2/ref/signals/#post-migrate
