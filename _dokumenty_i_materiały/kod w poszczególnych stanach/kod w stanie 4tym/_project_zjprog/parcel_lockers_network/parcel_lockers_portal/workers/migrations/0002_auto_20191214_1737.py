# Generated by Django 3.0 on 2019-12-14 17:37

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workers', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workermatrix',
            name='put_matrix',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(blank=True, default=0, null=True), size=64),
        ),
        migrations.AlterField(
            model_name='workermatrix',
            name='take_matrix',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(blank=True, default=0, null=True), size=64),
        ),
    ]
