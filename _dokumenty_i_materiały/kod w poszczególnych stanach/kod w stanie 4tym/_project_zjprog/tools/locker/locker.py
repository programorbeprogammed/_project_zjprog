import RPi.GPIO as GPIO
from time import sleep  # TODO make it non blocking


ALLOWED_PINS = [22, 27]
MAX_LOCK_OPEN_TIME_s = 5   # TODO for safety, em lock cannot be open longer

class Locker:
    def __init__(self, pin_no_BCM):
        if pin_no_BCM in ALLOWED_PINS:
            self.pin = pin_no_BCM
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self.pin, GPIO.OUT)


    def open(self, duration_s=1):
        try:
            GPIO.output(self.pin, GPIO.LOW)
            sleep(duration_s)
            self.close()
        except Exception as e:
            print(f'GPIO error, pin not allowed.\n{e}')
    def close(self):
        try:
            GPIO.output(self.pin, GPIO.HIGH)
        except Exception as e:
            print(f'GPIO error, pin not allowed.\n{e}')

