from time import sleep
# import RPi.GPIO as GPIO
import json
from kivy.app import App
from kivy.uix.button import Button, Label
from kivy.uix.popup import Popup
from kivy.uix.scrollview import ScrollView
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.slider import Slider
from kivy.clock import Clock
from kivy.graphics import Color, Rectangle

from kivy.uix.widget import Widget
from kivy.uix.pagelayout import PageLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen, SwapTransition, CardTransition, FadeTransition
from kivy.lang.builder import Builder

# from kivy.config import Config
# Config.set('graphics', 'fullscreen', 'auto')

commands = {
    'worker_credentials': 0,
    'register_parcel_machine': 1,
    'user_receive_package': 2,
    'package_was_taken': 3,
    'worker_open_all_boxes': 4,
    'worker_finished': 5,
    'user_send_package': 6,
}


def check_package_nr(package_nr):
    print(f'package_nr = {package_nr}')
    req_dict = {
        'command': commands['user_send_package'],
        'package_nr': package_nr,
    }
    req = json.dumps(req_dict, ensure_ascii=False)
    res = send_req(req)
    if json.loads(res)['response'] == 'OK':
        return 'OK'
    else:
        return 'NOK'


def input_error(error, text_entry):
    print("INPUT error")
    content = BoxLayout(orientation='vertical')
    scrollview = ScrollView()

    close_popup = Button(text='Close this popup')
    error_message = Label(text=str(error))

    scrollview.add_widget(error_message)
    content.add_widget(scrollview)
    content.add_widget(close_popup)

    popup = Popup(title='An error occured',
                  content=content, size_hint=(.8, .8))

    close_popup.bind(on_release=popup.dismiss)
    popup.open()


class Keyboard(GridLayout):

    def delete(self, *args):
        self.ids.text_input.text = self.ids.text_input.text[:-1]

    def get_input(self, *args):
        text = self.ids.text_input.text
        if text != '':
            if text[0] in '1234567890':
                try:
                    return str(text)
                except Exception as error:
                    input_error(error, text)
                    pass


def send_req(req):  # TODO mock function, implement sending request to server
    return json.dumps({'response': 'OK', }, ensure_ascii=False)


class IdleScreen(Screen):  # TODO create factory
    pass


class PackageNrInputScreen(Screen):
    pass


class ReceiveNrInputScreen(Screen):
    pass


class BoxOpenScreen(Screen):
    pass


class BoxClosedScreen(Screen):
    pass


gui = Builder.load_file("guimain.kv")
screen_manager = ScreenManager(transition=FadeTransition())
screen_manager.add_widget(IdleScreen(name="IdleScreen"))
screen_manager.add_widget(PackageNrInputScreen(name="PackageNrInputScreen"))
screen_manager.add_widget(ReceiveNrInputScreen(name="ReceiveNrInputScreen"))
# screen_manager.get_screen('ReceiveNrInputScreen').add_widget(Keyboard())
screen_manager.add_widget(BoxOpenScreen(name="BoxOpenScreen"))
screen_manager.add_widget(BoxClosedScreen(name="BoxClosedScreen"))


class GuiMain(App):
    def build(self):
        self.manager = screen_manager
        return self.manager

    def on_quit(self):
        print('quitting now')
        exit()

    def send_package(self):
        package_nr = self.manager.get_screen('PackageNrInputScreen').ids['"kb"'].get_input()
        return check_package_nr(package_nr)

    def receive_package(self):
        package_nr = self.manager.get_screen('ReceiveNrInputScreen').ids['"kb"'].get_input()
        return check_package_nr(package_nr)


if __name__ == '__main__':
    app = GuiMain()
    app.run()
    # The ScreenManager controls moving between screens
    # Add the screens to the manager and then supply a name
    # that is used to switch screens
