from django.apps import AppConfig


class ParcelServiceConfig(AppConfig):
    name = 'parcel_service'
