from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']

class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']

"""
ogolnie sam update obrazka mozna przerobic na obrazek bedący sumą kontrolna lub znakiem wodnym przy loginie (typu obrazek przy loginie do ipko.pl)

lub (i to jest pomysł lepszy) może też iść razem w komunikacie mailowym jako zabezpieczenie, że jest to email autentyczny od operatora serwisu,
ale musiałby byc jakoś zabezpieczony w mailu przed przejęciem obrazka w wiadomosci mailowej, obrazek mógłby byc tłem kodu QR nadesłanym w mailu,
kod QR ma na środku znak firmowy zasłaniący część obrazka,

https://www.ilovefreesoftware.com/27/webware/color-qr-code-generator-with-logo-and-background-image.html
http://blog.kangaderoo.nl/2013/09/qrbuntu.html
"""
