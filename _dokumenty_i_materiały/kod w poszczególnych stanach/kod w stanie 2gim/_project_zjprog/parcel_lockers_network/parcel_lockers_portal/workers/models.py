from django.db import models
from django.contrib.postgres.fields import ArrayField

# Zarys nowego modelu aplikacji pracowników
class WorkerMatrix(models.Model):

    take_matrix = ArrayField(models.FloatField(null=True, blank=True),size=64)

    put_matrix = ArrayField(models.FloatField(null=True, blank=True),size=64)

    def take_parcel():
        paczki = Parcel.objects.filter(order_taken_by_employee_id=id)
        i=0
        for line in paczki.parcel_locket_number:
            take_matrix[i]= line

    def put_parcels():
        paczki = Parcel.objects.filter(order_send_by_employee_id=id)
        i=0
        for line in paczki.parcel_locket_number:
            put_matrix[i]= line
