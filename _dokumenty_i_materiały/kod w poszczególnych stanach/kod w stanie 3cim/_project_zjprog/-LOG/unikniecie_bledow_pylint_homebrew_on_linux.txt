(base) user@user-350V5C-351V5C-3540VC-3440VC:~$ brew install pylint
Updating Homebrew...
==> Auto-updated Homebrew!
Updated 2 taps (homebrew/core and homebrew/cask).
==> New Formulae
detekt              fastlane            powerline-go        skymaker
==> Updated Formulae
abcmidi                    hugo                       poppler
asio                       ios-deploy                 protobuf
atlantis                   jadx                       pulumi
awscli                     jboss-forge                pushpin
bit                        jenkins                    py2cairo
bitrise                    just                       pyenv
bumpversion                kakoune                    qemu
bup                        kubernetes-cli             r
certbot                    landscaper                 rex
conan                      libphonenumber             sbt
contentful-cli             libssh                     scrcpy
convox                     mesa                       serverless
coq                        minikube                   soci
crystal                    mono                       sonobuoy
diff-pdf                   msgpack                    srt
diffr                      navi                       starship
dnscrypt-proxy             ncmpc                      stress-ng
doctl                      neomutt                    sysbench
dvc                        netlify-cli                tektoncd-cli
erlang                     newlisp                    teleport
exploitdb                  node                       terraform
faust                      node@10                    terragrunt
flow                       now-cli                    terrahub
fribidi                    nsd                        tile38
frotz                      nss                        tomcat
fuseki                     numpy@1.16                 tomcat@8
fx                         ocaml-num                  tor
gatsby-cli                 opencv@2                   traefik
geogram                    operator-sdk               traefik@1
geographiclib              osc                        unbound
git                        osm2pgsql                  unrar
git-plus                   paket                      v8
glib-networking            pandoc                     vim
glooctl                    pandoc-citeproc            vips
gnome-autoar               pdftoipe                   wxmaxima
gnuradio                   pdnsrec                    xctool
go@1.12                    pgpool-ii                  yarn
gtk-mac-integration        phpstan
helmsman                   ponyc
==> Deleted Formulae
aap             bubbros         curlish         euca2ools       nicotine-plus
archivemail     bzr-fastimport  cvs2svn         ino             tnote
borg            closure-linter  ddar            muttils

==> Installing dependencies for pylint: gdbm, openssl@1.1, readline, sqlite, xz, bzip2, libffi and python
==> Installing pylint dependency: gdbm
==> Downloading https://linuxbrew.bintray.com/bottles/gdbm-1.18.1.x86_64_linux.b
==> Downloading from https://akamai.bintray.com/80/80fc4bdfef26e3df4c1f7390e1169
######################################################################## 100.0%
==> Pouring gdbm-1.18.1.x86_64_linux.bottle.1.tar.gz
🍺  /home/linuxbrew/.linuxbrew/Cellar/gdbm/1.18.1: 40 files, 1MB
==> Installing pylint dependency: openssl@1.1
==> Downloading https://linuxbrew.bintray.com/bottles/openssl@1.1-1.1.1d_1.x86_6
==> Downloading from https://akamai.bintray.com/01/010a4e5257eb685ac700f495208be
######################################################################## 100.0%
==> Pouring openssl@1.1-1.1.1d_1.x86_64_linux.bottle.tar.gz
==> Downloading https://curl.haxx.se/ca/cacert-2019-10-16.pem
######################################################################## 100.0%
==> Caveats
A CA file has been bootstrapped using certificates from the system
keychain. To add additional certificates, place .pem files in
  /home/linuxbrew/.linuxbrew/etc/openssl@1.1/certs

and run
  /home/linuxbrew/.linuxbrew/opt/openssl@1.1/bin/c_rehash

openssl@1.1 is keg-only, which means it was not symlinked into /home/linuxbrew/.linuxbrew,
because this is an alternate version of another formula.

If you need to have openssl@1.1 first in your PATH run:
  echo 'export PATH="/home/linuxbrew/.linuxbrew/opt/openssl@1.1/bin:$PATH"' >> ~/.bash_profile

For compilers to find openssl@1.1 you may need to set:
  export LDFLAGS="-L/home/linuxbrew/.linuxbrew/opt/openssl@1.1/lib"
  export CPPFLAGS="-I/home/linuxbrew/.linuxbrew/opt/openssl@1.1/include"

For pkg-config to find openssl@1.1 you may need to set:
  export PKG_CONFIG_PATH="/home/linuxbrew/.linuxbrew/opt/openssl@1.1/lib/pkgconfig"

==> Summary
🍺  /home/linuxbrew/.linuxbrew/Cellar/openssl@1.1/1.1.1d_1: 7,985 files, 21.2MB
==> Installing pylint dependency: readline
==> Downloading https://linuxbrew.bintray.com/bottles/readline-8.0.1.x86_64_linu
==> Downloading from https://akamai.bintray.com/72/7219d088b8500a4db8fa155b3cb1e
######################################################################## 100.0%
==> Pouring readline-8.0.1.x86_64_linux.bottle.tar.gz
🍺  /home/linuxbrew/.linuxbrew/Cellar/readline/8.0.1: 50 files, 1.9MB
==> Installing pylint dependency: sqlite
==> Downloading https://linuxbrew.bintray.com/bottles/sqlite-3.30.1.x86_64_linux
==> Downloading from https://akamai.bintray.com/95/954d7c6aac9b3e72c24b040c9e9e8
######################################################################## 100.0%
==> Pouring sqlite-3.30.1.x86_64_linux.bottle.tar.gz
🍺  /home/linuxbrew/.linuxbrew/Cellar/sqlite/3.30.1: 13 files, 4.8MB
==> Installing pylint dependency: xz
==> Downloading https://linuxbrew.bintray.com/bottles/xz-5.2.4.x86_64_linux.bott
==> Downloading from https://akamai.bintray.com/84/843e87c20f1d261e4fd08ed5856f7
######################################################################## 100.0%
==> Pouring xz-5.2.4.x86_64_linux.bottle.tar.gz
🍺  /home/linuxbrew/.linuxbrew/Cellar/xz/5.2.4: 99 files, 3.3MB
==> Installing pylint dependency: bzip2
==> Downloading https://linuxbrew.bintray.com/bottles/bzip2-1.0.8.x86_64_linux.b
==> Downloading from https://akamai.bintray.com/e5/e5fce257b9fee119d28d0e7f7dac9
######################################################################## 100.0%
==> Pouring bzip2-1.0.8.x86_64_linux.bottle.tar.gz
🍺  /home/linuxbrew/.linuxbrew/Cellar/bzip2/1.0.8: 30 files, 549.3KB
==> Installing pylint dependency: libffi
==> Downloading https://linuxbrew.bintray.com/bottles/libffi-3.2.1.x86_64_linux.
######################################################################## 100.0%
==> Pouring libffi-3.2.1.x86_64_linux.bottle.tar.gz
🍺  /home/linuxbrew/.linuxbrew/Cellar/libffi/3.2.1: 17 files, 362.4KB
==> Installing pylint dependency: python
==> Downloading https://linuxbrew.bintray.com/bottles/python-3.7.5.x86_64_linux.
==> Downloading from https://akamai.bintray.com/47/4709a9a0c66456028dc3a3f7fa5d5
######################################################################## 100.0%
==> Pouring python-3.7.5.x86_64_linux.bottle.tar.gz
==> /home/linuxbrew/.linuxbrew/Cellar/python/3.7.5/bin/python3 -s setup.py --no-
==> /home/linuxbrew/.linuxbrew/Cellar/python/3.7.5/bin/python3 -s setup.py --no-
==> /home/linuxbrew/.linuxbrew/Cellar/python/3.7.5/bin/python3 -s setup.py --no-
==> Caveats
Python has been installed as
  /home/linuxbrew/.linuxbrew/bin/python3

Unversioned symlinks `python`, `python-config`, `pip` etc. pointing to
`python3`, `python3-config`, `pip3` etc., respectively, have been installed into
  /home/linuxbrew/.linuxbrew/opt/python/libexec/bin

If you need Homebrew's Python 2.7 run
  brew install python@2

You can install Python packages with
  pip3 install <package>
They will install into the site-package directory
  /home/linuxbrew/.linuxbrew/lib/python3.7/site-packages

See: https://docs.brew.sh/Homebrew-and-Python
==> Summary
🍺  /home/linuxbrew/.linuxbrew/Cellar/python/3.7.5: 3,625 files, 66.8MB
==> Installing pylint
==> Downloading https://linuxbrew.bintray.com/bottles/pylint-2.4.4.x86_64_linux.
==> Downloading from https://akamai.bintray.com/51/5132776cfd7e429cb35eeaabf9294
######################################################################## 100.0%
==> Pouring pylint-2.4.4.x86_64_linux.bottle.tar.gz
🍺  /home/linuxbrew/.linuxbrew/Cellar/pylint/2.4.4: 777 files, 9.3MB
==> Caveats
==> openssl@1.1
A CA file has been bootstrapped using certificates from the system
keychain. To add additional certificates, place .pem files in
  /home/linuxbrew/.linuxbrew/etc/openssl@1.1/certs

and run
  /home/linuxbrew/.linuxbrew/opt/openssl@1.1/bin/c_rehash

openssl@1.1 is keg-only, which means it was not symlinked into /home/linuxbrew/.linuxbrew,
because this is an alternate version of another formula.

If you need to have openssl@1.1 first in your PATH run:
  echo 'export PATH="/home/linuxbrew/.linuxbrew/opt/openssl@1.1/bin:$PATH"' >> ~/.bash_profile

For compilers to find openssl@1.1 you may need to set:
  export LDFLAGS="-L/home/linuxbrew/.linuxbrew/opt/openssl@1.1/lib"
  export CPPFLAGS="-I/home/linuxbrew/.linuxbrew/opt/openssl@1.1/include"

For pkg-config to find openssl@1.1 you may need to set:
  export PKG_CONFIG_PATH="/home/linuxbrew/.linuxbrew/opt/openssl@1.1/lib/pkgconfig"

==> python
Python has been installed as
  /home/linuxbrew/.linuxbrew/bin/python3

Unversioned symlinks `python`, `python-config`, `pip` etc. pointing to
`python3`, `python3-config`, `pip3` etc., respectively, have been installed into
  /home/linuxbrew/.linuxbrew/opt/python/libexec/bin

If you need Homebrew's Python 2.7 run
  brew install python@2

You can install Python packages with
  pip3 install <package>
They will install into the site-package directory
  /home/linuxbrew/.linuxbrew/lib/python3.7/site-packages

See: https://docs.brew.sh/Homebrew-and-Python
(base) user@user-350V5C-351V5C-3540VC-3440VC:~$ 
