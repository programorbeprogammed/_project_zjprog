from django.conf import settings
from django.db import models


from django.contrib.auth.models import AbstractUser
from .models import Profile, WorkerMatrix



class ParcelMachineAddress(models.Model):
    city = models.CharField(max_length=100, null=True, blank=True)
    zip_postcode = models.CharField(max_length=20, null=True, blank=True)
    street_name = models.CharField(max_length=200, null=True, blank=True)
    street_number = models.CharField(max_length=10, null=True, blank=True)
    parcel_machine_address_label = models.CharField(max_length=100, null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)


class Address(models.Model):
    city = models.CharField(max_length=100, null=True, blank=True)
    zip_postcode = models.CharField(max_length=20, null=True, blank=True)
    street_name = models.CharField(max_length=200, null=True, blank=True)
    street_number = models.CharField(max_length=10, null=True, blank=True)
    parcel_machine_address_label = models.CharField(max_length=100, null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)


CURRENCY_CHOICES = [
    ('PLN', 'Złoty'),
    ('USD', 'Dolar amerykański'),
    ('EUR', 'Euro'),
]

class ShippingInsurance(models.Model):
    additional_insurance_price_value = models.IntegerField(null=True)
    additional_insurance_prive_currency = models.CharField(choices=CURRENCY_CHOICES, max_length=50)


DELIVERY_STATUS_DESCRIPTION = [
    ('Odebrana', 'Odebrana'),
    ('Do odbioru', 'Do odbioru'),
    ('Przesyłka uszkodzona', 'Przesyłka uszkodzona'),
    ('Przesyłka zgubiona', 'Przesyłka zgubiona'),
    ('Przesyłka w trasie', 'Przesyłka w trasie'),
]
class DeliveryStatus(models.Model):
    delivery_status_description = models.CharField(choices=DELIVERY_STATUS_DESCRIPTION, max_length=50)
    is_insured = models.ForeignKey(ShippingInsurance, on_delete=models.CASCADE, default=None, blank=True, null=True)


PACKAGE_TYPE_CHOICES = [
    ('Mała paczka X1xY1xZ1', 'Mała paczka X1xY1xZ1'),
    ('Średnia paczka X2xY2xZ2', 'Średnia paczka X2xY2xZ2'),
    ('Duża paczka X3xY3xZ3', 'Duża paczka X3xY3xZ3')
]


class Employees(models.Model):
    #packages_which_worker_have = models.ForeignKey(Parcel, Address, on_delete=models.CASCADE, default=None, blank=True, null=True) ##tu musi być  możliwośc odszukania paczek u pracownika
    employee_address_id = models.ForeignKey(Address, on_delete=models.CASCADE, default=None, blank=True, null=True)
    employee_internal_id_number = models.BigIntegerField(null=True)
    employee_name_plus_surname = models.CharField(max_length=50)
    employee_phone = models.CharField(max_length=50)
    other_employee_details = models.CharField(max_length=300)

class Parcel(models.Model):
    parcel_number = models.BigIntegerField(null=True)
    user_id = models.ForeignKey(Profile, on_delete=models.DO_NOTHING)
    order_taken_by_employee_id = models.ForeignKey(Employees, on_delete=models.CASCADE, default=None, blank=True, null=True)
    order_send_by_employee_id = models.ForeignKey(Employees, on_delete=models.CASCADE, default=None, blank=True, null=True)
    package_status_code = models.ForeignKey(DeliveryStatus, on_delete=models.CASCADE, default=None, blank=True, null=True)
    package_details = models.CharField(max_length=300)
    internal_package_flag = models.BooleanField(default=True) # internal_package_flag mówi o tym czy paczka jest z systemu paczkomatów czy ze świata
    kod_nadania = models.CharField(max_length=10)
    kod_odbioru = models.CharField(max_length=10)
    payment_status = models.BooleanField(default=True)
    final_address = models.ForeignKey(ParcelMachineAddress, on_delete=models.CASCADE, default=None, blank=True, null=True)
    parcel_locket_number = models.IntegerField(null=True)

class DeliveryRouteLocation(models.Model):
    location_address_id = models.ForeignKey(Address, on_delete=models.CASCADE, default=None, blank=True, null=True)
    location_name = models.CharField(max_length=100)
    other_location_details = models.CharField(max_length=100)
