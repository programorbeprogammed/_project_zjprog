
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='parcel_service-home'),
    path('about/', views.about, name='parcel_service-about'),
]
