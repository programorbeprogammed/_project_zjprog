from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    #jeśli user jest usunięty to usuwany jest profile; jeśli usunięty jest profile to nie jest usunięty user
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    #instancja
    def __str__(self):
        return f'Konto {self.user.username}'
