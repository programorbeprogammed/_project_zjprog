
"""
 chcemy przekierować użytkownika do homepage
 po zalogowaniu - aby to wykonac potrzeba redirect, stąd
 from django.shortcuts import render, redirect
"""

from django.shortcuts import render, redirect
#from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm



# Create your views here.
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Twoje konto zostało stworzone! Możesz się teraz zalogować.')
            return redirect('_login_')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})


# f''  f-string dla pythona 3.6 wzwyż, jeśli poniżej to formatted string

#decorator dodaje funkcjonalnośc do istniejącej funkcji
@login_required
def profile(request):

    """
    aby wypełnic poniższe forms danymi można stworzyć i podać instancję obiektu modelu którego te forms się spodziewają,
    spodziewaja się danych zalogowanego usera
    https://duckduckgo.com/?q=pupulate+forms+django&ia=web
    """

    user_update_form = UserUpdateForm(instance=request.user)
    profile_update_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
    'user_update_form': user_update_form,
    'profile_update_form': profile_update_form
    }

    return render(request, 'users/profile.html')



#message.debug
#message.info
#message.success
#message.warning
#message.error



# tymczasowa nazwa //parcel_service-home// to url pattern
# w aplikacji do homepage który muszę opracować (parcel_service/urls.py)
